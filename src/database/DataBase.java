package database;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

import user.Admin;
import user.Person;
import user.User;

public class DataBase {

    private Map<String, Person> accounts;

    public DataBase() throws IOException, ClassNotFoundException {
        createFile();
        getAccounts();
    }

    private void createFile() throws IOException {
        File file = new File("accounts.ser");
        if (!file.exists()) {
            file.createNewFile();
        }
    }

    private void getAccounts() throws IOException, ClassNotFoundException {
        accounts = new HashMap<>();
        accounts.put("admin", new Admin("admin", 123));
        FileInputStream file = null;
        ObjectInputStream object = null;
        file = new FileInputStream(new File("accounts.ser"));
        if (file.available() > 0) {
            object = new ObjectInputStream(file);
            Person user = null;
            do {
                try {
                    user = (Person) object.readObject();
                    accounts.put(user.getUserName(), user);
                } catch (Exception e) {
                    user = null;
                }
            } while (user != null);
        }
        if (object != null) {
            object.close();
        }
        if (file != null) {
            file.close();
        }
    }

    public void saveAccounts() throws IOException {
        accounts.remove("admin");
        FileOutputStream file = new FileOutputStream("accounts.ser");
        ObjectOutputStream object = new ObjectOutputStream(file);

        for (Person user : accounts.values()) {
            object.writeObject(user);
        }

        if (object != null) {
            object.close();
        }
        if (file != null) {
            file.close();
        }
    }

    public String getUsers() {
        StringBuilder users = new StringBuilder();
        if (accounts.values().isEmpty()) {
            return "";
        }
        for (Person user : accounts.values()) {
            users.append(user.getUserName() + "\n");
        }
        return users.toString();
    }

    public boolean contains(String userName) {
        return accounts.containsKey(userName);
    }

    public boolean checkIfSamePassword(String user, String password) {
        return (accounts.get(user).getPassword() == (hashPassword(user, password)));
    }

    public void addUser(String userName, String password) throws IllegalArgumentException {
        accounts.put(userName, new User(userName, hashPassword(userName, password)));
    }

    public void deleteUser(String name) {
        accounts.remove(name);
    }

    public Person getUser(String name) {
        return accounts.get(name);
    }

    private int hashPassword(String name, String password) {
        String text = name + password;
        int mod = 100_007;
        int base = 47;
        int result = 1;
        for (int i = 0; i < text.length(); i++) {
            result = (int) (((long) result) * base + (text.charAt(i) - 'a')) % mod;
        }
        return result;
    }
}
