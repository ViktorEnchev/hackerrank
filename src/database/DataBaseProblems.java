package database;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import problem.Problem;

public class DataBaseProblems {

    private int id;
    private Map<Integer, Problem> problems;
    private Map<String, Set<Integer>> tags;

    public DataBaseProblems() throws IOException, ClassNotFoundException {
        initialiseId();
        createFile("problems.ser");
        getProblems();
    }

    private void initialiseId() throws IOException {
        createFile("id.txt");
        @SuppressWarnings("resource")
        BufferedReader fileReader = new BufferedReader(new FileReader(new File("id.txt")));
        String result = "";
        if (fileReader.ready()) {
            result = fileReader.readLine();
        } else {
            result = "1";
        }
        id = Integer.parseInt(result);
    }

    private void createFile(String name) throws IOException {
        File file = new File(name);
        if (!file.exists()) {
            file.createNewFile();
        }
    }

    private void getProblems() throws IOException {
        problems = new HashMap<>();
        tags = new HashMap<>();
        FileInputStream file = null;
        ObjectInputStream object = null;
        file = new FileInputStream(new File("problems.ser"));
        if (file.available() > 0) {
            object = new ObjectInputStream(file);
            Problem problem = null;
            do {
                try {
                    problem = (Problem) object.readObject();
                    addProblemToMemory(problem);
                } catch (Exception e) {
                    problem = null;
                }
            } while (problem != null);
        }
        if (object != null) {
            object.close();
        }
        if (file != null) {
            file.close();
        }
    }

    public void addProblem(List<String> problemDescription) {
        Problem problem = new Problem(id++, problemDescription);
        addProblemToMemory(problem);
    }

    private void addProblemToMemory(Problem problem) {
        String problemTag = problem.getTag();
        int problemId = problem.getId();
        problems.put(problemId, problem);
        if (!tags.containsKey(problemTag)) {
            tags.put(problemTag, new HashSet<>());
        }
        tags.get(problemTag).add(problemId);
    }

    public void saveProblems() throws IOException {
        FileOutputStream file = new FileOutputStream("problems.ser");
        ObjectOutputStream object = new ObjectOutputStream(file);

        for (Problem problem : problems.values()) {
            object.writeObject(problem);
        }

        if (object != null) {
            object.close();
        }
        if (file != null) {
            file.close();
        }

        saveIdNuumber();
    }

    private void saveIdNuumber() throws IOException {
        BufferedWriter fileWrite = new BufferedWriter(new FileWriter(new File("id.txt")));
        fileWrite.write(String.format("%d", id));
        fileWrite.close();
    }

    public String showAllProblems() {
        StringBuilder result = new StringBuilder();
        for (Problem problem : problems.values()) {
            result.append(problem.getSmallDescription());
        }
        return result.toString();
    }

    public String showProblemByID(int problemId) {
        return problems.get(problemId).getFullDescription();
    }

    public String showProblemsByTag(String problemTag) {
        StringBuilder result = new StringBuilder();
        for (Integer integer : tags.get(problemTag)) {
            result.append(problems.get(integer).getSmallDescription());
        }
        return result.toString();
    }

    public void deleteProblem(int problemId) {
        String problemTag = problems.get(problemId).getTag();
        problems.remove(problemId);
        tags.get(problemTag).remove(problemId);
    }

    public boolean contains(int problemId) {
        return problems.containsKey(problemId);
    }

    public boolean constainsTag(String problemTag) {
        return tags.containsKey(problemTag);
    }

    public void setIdToOne() {
        id = 1;
    }

    public Problem getProblem(int problemId) {
        return problems.get(problemId);
    }
}
