package problem;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class Problem implements Serializable {

    private int id;
    private String tag;
    private String name;
    private String explanation;
    private String example;
    private String returnType;
    private String inputData;

    public Problem(int id, List<String> description) {
        this.id = id;
        tag = description.get(0);
        name = description.get(1);
        explanation = description.get(2);
        example = description.get(3);
        returnType = description.get(4);
        inputData = description.get(5);
    }

    public int getId() {
        return id;
    }

    public String getTag() {
        return tag;
    }

    public String getName() {
        return name;
    }

    public String getExplanation() {
        return explanation;
    }

    public String getExample() {
        return example;
    }

    public String getSmallDescription() {
        return id + "." + name + "(#" + tag + ")\n";
    }

    public String getFullDescription() {
        return getSmallDescription() + "\n" + explanation + "\n\n" + example + "\n";
    }

    public String getMethodDiscription() {
        return String.format("public %s %sMethod(%s)", returnType, name, inputData);
    }

    public String getCMethodDiscription() {
        return String.format("%s %sMethod(%s)", returnType, name, inputData);
    }

    public String getJavaClass(String solution) {
        StringBuilder sourceFile = new StringBuilder();
        sourceFile.append("import java.util.*;\n");
        sourceFile.append(String.format("public class %s {\n", name.split(" ")[0]));
        sourceFile.append(String.format("    %s {\n", getMethodDiscription()));
        sourceFile.append(solution);
        return sourceFile.toString();
    }

    public String getCPlusPlus(String solution) {
        StringBuilder sourceFile = new StringBuilder();
        sourceFile.append("#include <iostream>\nusing namespace std;\n");
        sourceFile.append(String.format("%s {\n", getCMethodDiscription()));
        sourceFile.append(solution);
        return sourceFile.toString();
    }
}
