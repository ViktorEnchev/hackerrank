package useroption;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import consolemethod.ConsoleInput;
import database.DataBase;
import database.DataBaseProblems;
import problem.Problem;
import solveproblem.SolveProblemCpp;
import solveproblem.SolveProblemJava;
import solveproblem.SolveProblemLanguage;
import user.Person;

public class SolveProblem extends UserOption {

    private final Map<String, SolveProblemLanguage> languages;

    public SolveProblem(ConsoleInput consoleInput) {
        super(consoleInput);
        languages = new HashMap<>();
        languages.put("java", new SolveProblemJava(consoleInput));
        languages.put("cpp", new SolveProblemCpp(consoleInput));
    }

    @Override
    public String executeOption(Person user, DataBase dataBase, DataBaseProblems dataBaseProblems,
            List<String> command) {
        if (command.size() < 3) {
            throw new IllegalArgumentException("Wrong input!");
        }
        String problemId = command.get(1);
        double number = 0;
        try {
            number = Double.parseDouble(problemId);
        } catch (Exception e) {
            throw new NumberFormatException("Not a number!");
        }
        int realId = (int) number;
        if (!dataBaseProblems.contains(realId)) {
            throw new IllegalArgumentException(String.format("Problem %d doesn't exist!", realId));
        }
        SolveProblemLanguage howToSolve = null;
        for (String string : languages.keySet()) {
            if (command.get(2).contains(string)) {
                howToSolve = languages.get(string);
                break;
            }
        }
        if (howToSolve == null) {
            throw new IllegalArgumentException(
                    String.format("solveProblem <id> <solution>(file or (java, cpp)", realId));
        }
        Problem problemToSolve = dataBaseProblems.getProblem(realId);
        String result = "";
        try {
            result = howToSolve.execute(problemToSolve, command);
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage());
        }
        return result;
    }
}
