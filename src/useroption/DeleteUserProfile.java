package useroption;

import java.util.List;

import consolemethod.ConsoleInput;
import database.DataBase;
import database.DataBaseProblems;
import user.Person;

public class DeleteUserProfile extends UserOption {

    public DeleteUserProfile(ConsoleInput consoleInput) {
        super(consoleInput);
    }

    @Override
    public String executeOption(Person user, DataBase dataBase, DataBaseProblems dataBaseProblems, List<String> command) {
        String name = consoleInput.typeText("User to delete: ");
        if (!dataBase.contains(name)) {
            throw new IllegalArgumentException(String.format("%s doesn't exist!", name));
        }
        dataBase.deleteUser(name);
        return String.format("%s has been deleted", name);
    }

}
