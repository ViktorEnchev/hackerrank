package useroption;

import java.util.List;

import consolemethod.ConsoleInput;
import database.DataBase;
import database.DataBaseProblems;
import user.Person;

public class ShowProblemByTag extends UserOption {

    public ShowProblemByTag(ConsoleInput consoleInput) {
        super(consoleInput);
    }

    @Override
    public String executeOption(Person user, DataBase dataBase, DataBaseProblems dataBaseProblems,
            List<String> command) {
        if (command.size() < 2) {
            throw new IllegalArgumentException("Wrong input!");
        }
        String problemTag = command.get(1);
        if (!dataBaseProblems.constainsTag(problemTag)) {
            throw new IllegalArgumentException(String.format("There are no %s problems!", problemTag));
        }
        return dataBaseProblems.showProblemsByTag(problemTag);
    }

}
