package useroption;

import java.util.List;

import consolemethod.ConsoleInput;
import database.DataBase;
import database.DataBaseProblems;
import user.Person;

public class DeleteProblem extends UserOption {

    public DeleteProblem(ConsoleInput consoleInput) {
        super(consoleInput);
    }

    @Override
    public String executeOption(Person user, DataBase dataBase, DataBaseProblems dataBaseProblems, List<String> command)
            throws NumberFormatException {
        if (command.size() < 2) {
            throw new IllegalArgumentException("Wrong input!");
        }
        String problemId = command.get(1);
        double number = 0;
        try {
            number = Double.parseDouble(problemId);
        } catch (Exception e) {
            throw new NumberFormatException("Not a number!");
        }
        int realId = (int) number;
        if (!dataBaseProblems.contains(realId)) {
            throw new IllegalArgumentException(String.format("Problem %d doesn't exist!", realId));
        }
        dataBaseProblems.deleteProblem(realId);
        return String.format("Problem %s has been deleted", realId);
    }

}
