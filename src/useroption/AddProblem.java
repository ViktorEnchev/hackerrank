package useroption;

import java.util.Arrays;
import java.util.List;

import consolemethod.ConsoleInput;
import database.DataBase;
import database.DataBaseProblems;
import user.Person;

public class AddProblem extends UserOption {

    public AddProblem(ConsoleInput consoleInput) {
        super(consoleInput);
    }

    @Override
    public String executeOption(Person user, DataBase dataBase, DataBaseProblems dataBaseProblems,
            List<String> command) {
        String tag = consoleInput.typeText("Tag: ");
        String name = consoleInput.typeText("Name: ");
        String explanation = consoleInput.typeText("Explanation: ");
        String example = consoleInput.typeText("Example: ");
        String returnType = consoleInput.typeText("Return Type: ");
        String inputData = consoleInput.typeText("Input Data: ");
        dataBaseProblems.addProblem(Arrays.asList(tag, name, explanation, example, returnType, inputData));
        return "Problem added to data base";
    }

}
