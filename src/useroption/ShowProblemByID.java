package useroption;

import java.util.List;

import consolemethod.ConsoleInput;
import database.DataBase;
import database.DataBaseProblems;
import user.Person;

public class ShowProblemByID extends UserOption {

    public ShowProblemByID(ConsoleInput consoleInput) {
        super(consoleInput);
    }

    @Override
    public String executeOption(Person user, DataBase dataBase, DataBaseProblems dataBaseProblems,
            List<String> command) {
        if (command.size() < 2) {
            throw new IllegalArgumentException("Wrong input!");
        }
        String problemId = command.get(1);
        double number = 0;
        try {
            number = Double.parseDouble(problemId);
        } catch (Exception e) {
            throw new NumberFormatException("Not a number!");
        }
        int realId = (int) number;
        if (!dataBaseProblems.contains(realId)) {
            throw new IllegalArgumentException(String.format("Problem %d doesn't exist!", realId));
        }
        return dataBaseProblems.showProblemByID(realId);
    }

}
