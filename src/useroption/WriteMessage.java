package useroption;

import java.util.List;

import consolemethod.ConsoleInput;
import database.DataBase;
import database.DataBaseProblems;
import user.Person;

public class WriteMessage extends UserOption {

    public WriteMessage(ConsoleInput consoleInput) {
        super(consoleInput);
    }

    @Override
    public String executeOption(Person user, DataBase dataBase, DataBaseProblems dataBaseProblems, List<String> command) {
        if (command.size() < 3) {
            throw new IllegalArgumentException("Wrong input!");
        }
        if (!dataBase.contains(command.get(1))) {
            throw new IllegalArgumentException(String.format("%s doesn't exist!", command.get(1)));
        }
        Person resever = dataBase.getUser(command.get(1));
        String message = "";
        for (int i = 2; i < command.size(); i++) {
            message += command.get(i);
        }
        resever.setMessage(user.getUserName(), message);
        return "Message sent";
    }
}
