package useroption;

import java.util.List;

import consolemethod.ConsoleInput;
import database.DataBase;
import database.DataBaseProblems;
import user.Person;

public class LogOut extends UserOption {

    public LogOut(ConsoleInput consoleInput) {
        super(consoleInput);
    }

    @Override
    public String executeOption(Person user, DataBase dataBase, DataBaseProblems dataBaseProblems, List<String> command) {
        String name = user.getUserName();
        String.format("%s has been logged out", name);
        return "logout";
    }
}
