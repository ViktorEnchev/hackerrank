package useroption;

import java.util.List;

import consolemethod.ConsoleInput;
import consolemethod.ConsoleOutput;
import database.DataBase;
import database.DataBaseProblems;
import user.Person;

public class DeleteMyProfile extends UserOption {

    public DeleteMyProfile(ConsoleInput consoleInput) {
        super(consoleInput);
    }

    @Override
    public String executeOption(Person user, DataBase dataBase, DataBaseProblems dataBaseProblems, List<String> command) {
        String name = user.getUserName();
        if (!dataBase.contains(name)) {
            throw new IllegalArgumentException(String.format("%s doesn't exist!", name));
        }
        dataBase.deleteUser(name);
        ConsoleOutput.outputMessage(String.format("%s has been deleted", name));
        return "logout";
    }
}
