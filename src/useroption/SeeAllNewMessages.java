package useroption;

import java.util.List;

import consolemethod.ConsoleInput;
import database.DataBase;
import database.DataBaseProblems;
import user.Person;

public class SeeAllNewMessages extends UserOption {

    public SeeAllNewMessages(ConsoleInput consoleInput) {
        super(consoleInput);
    }

    @Override
    public String executeOption(Person user, DataBase dataBase, DataBaseProblems dataBaseProblems, List<String> command) {
        String unreadMessages = user.getAllUnreadMessages();
        return unreadMessages;
    }

}
