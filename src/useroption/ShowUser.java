package useroption;

import java.util.List;

import consolemethod.ConsoleInput;
import database.DataBase;
import database.DataBaseProblems;
import user.Person;

public class ShowUser extends UserOption {

    public ShowUser(ConsoleInput consoleInput) {
        super(consoleInput);
    }

    @Override
    public String executeOption(Person user, DataBase dataBase, DataBaseProblems dataBaseProblems, List<String> command) {
        if (command.size() < 2) {
            throw new IllegalArgumentException("Wrong input!");
        }
        if (!dataBase.contains(command.get(1))) {
            throw new IllegalArgumentException(String.format("%s doesn't exist!", command.get(1)));
        }

        return dataBase.getUser(command.get(1)).toString();
    }
}
