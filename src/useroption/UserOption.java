package useroption;

import java.util.List;

import consolemethod.ConsoleInput;
import database.DataBase;
import database.DataBaseProblems;
import user.Person;

public abstract class UserOption {

    protected ConsoleInput consoleInput;

    public UserOption(ConsoleInput consoleInput) {
        this.consoleInput = consoleInput;
    }

    public abstract String executeOption(Person user, DataBase dataBase, DataBaseProblems dataBaseProblems,
            List<String> command);

}
