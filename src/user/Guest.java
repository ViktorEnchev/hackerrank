package user;

import java.io.Serializable;
import java.util.Random;

@SuppressWarnings("serial")
public class Guest implements Person, Serializable {

    private String userName;
    private String type;

    public Guest() {
        char[] name = new char[8];
        for (int i = 0; i < name.length; i++) {
            name[i] = (char) ((new Random().nextInt(26)) + 65);
        }
        userName = String.valueOf(name);
        type = "guest";
    }

    @Override
    public String getUserName() {
        return userName;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public int getPassword() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int showNumberOfUnreadMessages() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public String getAllUnreadMessages() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setMessage(String user, String message) {
        // TODO Auto-generated method stub
    }
}
