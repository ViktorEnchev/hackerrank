package user;

public interface Person {

    public String getUserName();

    public int getPassword();

    public String getType();

    public void setMessage(String user, String message);

    public int showNumberOfUnreadMessages();

    public String getAllUnreadMessages();

    public String toString();
}
