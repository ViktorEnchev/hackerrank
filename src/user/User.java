package user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class User implements Person, Serializable {
    private String userName;
    private int password;
    private String type;
    private Map<String, List<String>> messages;
    private Map<String, Integer> unreadMessages;

    public User(String userName, int password) {
        this.userName = userName;
        this.password = password;
        type = "user";
        messages = new HashMap<>();
        unreadMessages = new HashMap<>();
    }

    @Override
    public void setMessage(String user, String message) {
        if (messages.containsKey(user)) {
            messages.get(user).add(user + ":\n" + message + "\n\n");
            unreadMessages.put(user, unreadMessages.get(user) + 1);
        } else {
            messages.put(user, new ArrayList<>());
            messages.get(user).add(user + ":\n" + message + "\n\n");
            unreadMessages.put(user, 1);
        }
    }

    @Override
    public String getUserName() {
        return userName;
    }

    @Override
    public int getPassword() {
        return password;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public int showNumberOfUnreadMessages() {
        if (unreadMessages.size() > 0) {
            int sum = 0;
            for (Integer integer : unreadMessages.values()) {
                sum += integer;
            }
            return sum;
        }
        return 0;
    }

    @Override
    public String getAllUnreadMessages() {
        String result = "";
        for (String name : unreadMessages.keySet()) {
            int messageSize = messages.get(name).size();
            int unreadMessageSize = unreadMessages.get(name);
            int size = messageSize - unreadMessageSize;
            for (int i = 0; i < unreadMessageSize; i++) {
                result += messages.get(name).get(size + i);
            }
        }
        unreadMessages.clear();

        return result;
    }

    @Override
    public String toString() {
        return "Name: " + userName + "\n";
    }
}
