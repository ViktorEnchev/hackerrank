package main;

import java.io.IOException;

import database.DataBase;
import database.DataBaseProblems;
import hackerrank.HackerRank;

public class Main {

    public static void main(String[] args) {
        DataBase dataBase = null;
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBase = new DataBase();
            dataBaseProblems = new DataBaseProblems();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        HackerRank hackerRank = new HackerRank(dataBase, dataBaseProblems);
        hackerRank.hackerMenu();

        try {
            dataBase.saveAccounts();
            dataBaseProblems.saveProblems();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
