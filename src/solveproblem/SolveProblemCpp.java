package solveproblem;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import consolemethod.ConsoleInput;
import consolemethod.ConsoleOutput;
import problem.Problem;

public class SolveProblemCpp implements SolveProblemLanguage {

    private ConsoleInput consoleInput;

    public SolveProblemCpp(ConsoleInput consoleInput) {
        this.consoleInput = consoleInput;
    }

    @Override
    public String execute(Problem problemToSolve, List<String> command) {
        String result = "";
        String solution = command.get(2);
        if (solution.equals("cpp")) {
            result = CPlusPlusCode(problemToSolve);
        } else if (solution.endsWith(".cpp")) {
            result = CPlusPlusFile(problemToSolve, solution);
        } else {
            throw new IllegalArgumentException("Wrong input!");
        }
        return result;
    }

    private String CPlusPlusCode(Problem problemToSolve) {
        String result = "";
        ConsoleOutput.outputMessage("Solve this problem:\n" + problemToSolve.getCPlusPlus(""));
        try {
            result = getCPlusPlusCompilationResult(problemToSolve);
        } catch (IOException e) {
        }
        removeCPlusPlusFilesThatWereCompiled(result, problemToSolve.getName());
        return result;
    }

    private String getCPlusPlusCompilationResult(Problem problemToSolve) throws IOException {
        String solution = consoleInput.typeText("Your solution:\n");
        String cplusplusFile = problemToSolve.getCPlusPlus(solution);
        File file = new File(String.format("%s.cpp", problemToSolve.getName()));
        if (!file.exists()) {
            file.createNewFile();
        }
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
        bufferedWriter.write(String.format("%s\nint main(){\nreturn 0;}", cplusplusFile));
        bufferedWriter.close();
        Process p = Runtime.getRuntime().exec(
                String.format("g++ -o %s %s.cpp", problemToSolve.getName(), problemToSolve.getName()), null, null);
        while (p.isAlive()) {
        }
        if (new File(String.format("%s", problemToSolve.getName())).exists()) {
            return "Compilation is successful";
        }
        return "Compilation Failed";
    }

    private void removeCPlusPlusFilesThatWereCompiled(String result, String name) {
        File cplusplusFile = new File(String.format("%s.cpp", name));
        File cplusplusExe = null;
        if (result.equals("Compilation is successful")) {
            cplusplusExe = new File(String.format("%s", name));
            cplusplusExe.delete();
        }
        cplusplusFile.delete();
    }

    private String CPlusPlusFile(Problem problemToSolve, String cppFile) {
        if (!new File(cppFile).exists()) {
            throw new IllegalArgumentException(String.format("No file %s", cppFile));
        }
        Process p = null;
        try {
            p = Runtime.getRuntime().exec(
                    String.format("g++ -o %s %s", cppFile.substring(0, cppFile.length() - 4), cppFile), null, null);
        } catch (IOException e) {
        }
        while (p.isAlive()) {
        }
        if (new File(String.format("%s", cppFile.substring(0, cppFile.length() - 4))).exists()) {
            return "Compilation is successful";
        }
        return "Compilation Failed";
    }
}
