package solveproblem;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import consolemethod.ConsoleInput;
import consolemethod.ConsoleOutput;
import problem.Problem;

public class SolveProblemJava implements SolveProblemLanguage {

    private ConsoleInput consoleInput;

    public SolveProblemJava(ConsoleInput consoleInput) {
        this.consoleInput = consoleInput;
    }

    @Override
    public String execute(Problem problemToSolve, List<String> command) {
        String result = "";
        String solution = command.get(2);
        if (solution.equals("java")) {
            result = JavaCode(problemToSolve);
        } else if (solution.endsWith(".java")) {
            result = JavaFile(problemToSolve, solution);
        } else {
            throw new IllegalArgumentException("Wrong input!");
        }
        return result;
    }

    private String JavaCode(Problem problemToSolve) {
        String result = "";
        ConsoleOutput.outputMessage("Solve this problem:\n" + problemToSolve.getJavaClass(""));
        try {
            result = getJavaCompilationResult(problemToSolve);
        } catch (IOException e) {
        }
        removeJavaFilesThatWereCompiled(result, problemToSolve.getName());
        return result;
    }

    private String getJavaCompilationResult(Problem problemToSolve) throws IOException {
        String solution = consoleInput.typeText("Your solution:\n");
        String javaSourceFile = problemToSolve.getJavaClass(solution);
        File file = new File(String.format("%s.java", problemToSolve.getName()));
        if (!file.exists()) {
            file.createNewFile();
        }
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
        bufferedWriter.write(javaSourceFile);
        bufferedWriter.close();
        Process p = Runtime.getRuntime().exec(String.format("javac %s.java", problemToSolve.getName()), null, null);
        while (p.isAlive()) {
        }
        if (new File(String.format("%s.class", problemToSolve.getName())).exists()) {
            return "Compilation is successful";
        }
        return "Compilation Failed";
    }

    private void removeJavaFilesThatWereCompiled(String result, String name) {
        File javaFile = new File(String.format("%s.java", name));
        File javaClassFile = null;
        if (result.equals("Compilation is successful")) {
            javaClassFile = new File(String.format("%s.class", name));
            javaClassFile.delete();
        }
        javaFile.delete();
    }

    private String JavaFile(Problem problemToSolve, String javaFile) {
        if (!new File(javaFile).exists()) {
            throw new IllegalArgumentException(String.format("No file %s", javaFile));
        }
        Process p = null;
        try {
            p = Runtime.getRuntime().exec(String.format("javac %s", javaFile, null, null));
        } catch (IOException e) {
        }
        while (p.isAlive()) {
        }
        if (new File(String.format("%s.class", javaFile.replace(".java", ""))).exists()) {
            return "Compilation is successful";
        }
        return "Compilation Failed";
    }
}
