package solveproblem;

import java.util.List;

import problem.Problem;

public interface SolveProblemLanguage {

    public String execute(Problem problemToSolve, List<String> command);

}
