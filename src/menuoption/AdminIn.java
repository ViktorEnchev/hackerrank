package menuoption;

import consolemethod.ConsoleInput;
import database.DataBase;
import user.Person;

public class AdminIn extends MenuOption {

    public AdminIn(ConsoleInput consoleInput) {
        super(consoleInput);
    }

    @Override
    public Person executeOption(DataBase dataBase) {
        return dataBase.getUser("admin");
    }
}
