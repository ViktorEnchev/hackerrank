package menuoption;

import consolemethod.ConsoleInput;
import consolemethod.ConsoleOutput;
import database.DataBase;
import user.Person;

public class SingIn extends MenuOption {

    public SingIn(ConsoleInput consoleInput) {
        super(consoleInput);
    }

    @Override
    public Person executeOption(DataBase dataBase) {
        ConsoleOutput.outputMessage("Type quit to cancel");
        String name = consoleInput.typeText("User name: ");
        if (name.equals("quit")) {
            return null;
        }
        String password = consoleInput.typeText("Password: ");
        if (!dataBase.contains(name)) {
            throw new IllegalArgumentException(String.format("%s doesn't exist!", name));
        }
        if (!dataBase.checkIfSamePassword(name, password)) {
            throw new IllegalArgumentException("Wrong password!");
        }
        Person user = dataBase.getUser(name);

        ConsoleOutput.outputMessage(String.format("Welcome %s", user.getUserName()));
        return user;
    }
}
