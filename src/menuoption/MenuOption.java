package menuoption;

import consolemethod.ConsoleInput;
import database.DataBase;
import user.Person;

public abstract class MenuOption {
    protected ConsoleInput consoleInput;

    public MenuOption(ConsoleInput consoleInput) {
        this.consoleInput = consoleInput;
    }

    public abstract Person executeOption(DataBase dataBase);

}
