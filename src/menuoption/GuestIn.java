package menuoption;

import consolemethod.ConsoleInput;
import database.DataBase;
import user.Guest;
import user.Person;

public class GuestIn extends MenuOption {

    public GuestIn(ConsoleInput consoleInput) {
        super(consoleInput);
    }

    @Override
    public Person executeOption(DataBase dataBase) {
        return new Guest();
    }
}
