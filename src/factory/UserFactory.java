package factory;

import java.util.HashMap;

import consolemethod.ConsoleInput;
import useroption.AddProblem;
import useroption.DeleteMyProfile;
import useroption.LogOut;
import useroption.SeeAllNewMessages;
import useroption.ShowAllProblems;
import useroption.ShowProblemByID;
import useroption.ShowProblemByTag;
import useroption.ShowUser;
import useroption.SolveProblem;
import useroption.WriteMessage;

public class UserFactory extends Factory {

    public UserFactory(ConsoleInput consoleInput) {
        super(consoleInput);
    }

    @Override
    protected void setMap() {
        userOptions = new HashMap<>();
        userOptions.put("deleteMyProfile", new DeleteMyProfile(consoleInput));
        userOptions.put("show", new ShowUser(consoleInput));
        userOptions.put("writeMessage", new WriteMessage(consoleInput));
        userOptions.put("seeAllNewMessages", new SeeAllNewMessages(consoleInput));
        userOptions.put("addProblem", new AddProblem(consoleInput));
        userOptions.put("showAllProblems", new ShowAllProblems(consoleInput));
        userOptions.put("showProblemByID", new ShowProblemByID(consoleInput));
        userOptions.put("showProblemByTag", new ShowProblemByTag(consoleInput));
        userOptions.put("solveProblem", new SolveProblem(consoleInput));
        userOptions.put("logout", new LogOut(consoleInput));
        userOptions.put("quit", null);
    }
}
