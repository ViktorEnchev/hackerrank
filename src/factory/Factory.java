package factory;

import java.util.List;
import java.util.Map;

import consolemethod.ConsoleInput;
import useroption.UserOption;

public abstract class Factory {

    protected Map<String, UserOption> userOptions;
    protected ConsoleInput consoleInput;

    public Factory(ConsoleInput consoleInput) {
        this.consoleInput = consoleInput;
        setMap();
    }

    protected abstract void setMap();

    public UserOption getOption(List<String> command) {
        if (!userOptions.containsKey(command.get(0))) {
            throw new IllegalArgumentException("Wrong input!");
        }
        return userOptions.get(command.get(0));
    }

}
