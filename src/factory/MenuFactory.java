package factory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import consolemethod.ConsoleInput;
import menuoption.AdminIn;
import menuoption.GuestIn;
import menuoption.MenuOption;
import menuoption.SingIn;
import menuoption.SingUp;

public class MenuFactory {

    private Map<String, MenuOption> menuOptions;
    @SuppressWarnings("unused")
    private ConsoleInput consoleInput;

    public MenuFactory(ConsoleInput consoleInput) {
        menuOptions = new HashMap<>();
        this.consoleInput = consoleInput;
        menuOptions.put("singUp", new SingUp(consoleInput));
        menuOptions.put("singIn", new SingIn(consoleInput));
        menuOptions.put("guest", new GuestIn(consoleInput));
        menuOptions.put("admin", new AdminIn(consoleInput));
        menuOptions.put("quit", null);
    }

    public MenuOption getOption(List<String> command) {
        if (!menuOptions.containsKey(command.get(0))) {
            throw new IllegalArgumentException("Wrong input!");
        }
        return menuOptions.get(command.get(0));
    }
}
