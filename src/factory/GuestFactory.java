package factory;

import java.util.HashMap;

import consolemethod.ConsoleInput;
import useroption.LogOut;
import useroption.ShowAllProblems;
import useroption.ShowProblemByID;
import useroption.ShowProblemByTag;
import useroption.ShowUser;

public class GuestFactory extends Factory {

    public GuestFactory(ConsoleInput consoleInput) {
        super(consoleInput);
    }

    @Override
    protected void setMap() {
        userOptions = new HashMap<>();
        userOptions.put("show", new ShowUser(consoleInput));
        userOptions.put("showAllProblems", new ShowAllProblems(consoleInput));
        userOptions.put("showProblemByID", new ShowProblemByID(consoleInput));
        userOptions.put("showProblemByTag", new ShowProblemByTag(consoleInput));
        userOptions.put("logout", new LogOut(consoleInput));
        userOptions.put("quit", null);
    }
}
