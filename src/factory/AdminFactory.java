package factory;

import java.util.HashMap;

import consolemethod.ConsoleInput;
import useroption.AddProblem;
import useroption.DeleteProblem;
import useroption.DeleteUserProfile;
import useroption.LogOut;
import useroption.SeeAllNewMessages;
import useroption.ShowAllProblems;
import useroption.ShowProblemByID;
import useroption.ShowProblemByTag;
import useroption.ShowUser;
import useroption.SolveProblem;
import useroption.WriteMessage;

public class AdminFactory extends Factory {

    public AdminFactory(ConsoleInput consoleInput) {
        super(consoleInput);
    }

    @Override
    protected void setMap() {
        userOptions = new HashMap<>();
        userOptions.put("deleteUserProfile", new DeleteUserProfile(consoleInput));
        userOptions.put("show", new ShowUser(consoleInput));
        userOptions.put("writeMessage", new WriteMessage(consoleInput));
        userOptions.put("seeAllNewMessages", new SeeAllNewMessages(consoleInput));
        userOptions.put("addProblem", new AddProblem(consoleInput));
        userOptions.put("showAllProblems", new ShowAllProblems(consoleInput));
        userOptions.put("showProblemByID", new ShowProblemByID(consoleInput));
        userOptions.put("showProblemByTag", new ShowProblemByTag(consoleInput));
        userOptions.put("deleteProblem", new DeleteProblem(consoleInput));
        userOptions.put("solveProblem", new SolveProblem(consoleInput));
        userOptions.put("logout", new LogOut(consoleInput));
        userOptions.put("quit", null);
    }
}
