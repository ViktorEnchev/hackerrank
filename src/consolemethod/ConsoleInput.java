package consolemethod;

import java.util.Scanner;

public class ConsoleInput {

    private Scanner in = new Scanner(System.in);

    public String typeText(String text) {
        System.out.print(text);
        String name = in.nextLine();
        return name;
    }
}
