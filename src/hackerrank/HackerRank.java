package hackerrank;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import consolemethod.ConsoleInput;
import consolemethod.ConsoleOutput;
import database.DataBase;
import database.DataBaseProblems;
import factory.AdminFactory;
import factory.Factory;
import factory.GuestFactory;
import factory.MenuFactory;
import factory.UserFactory;
import menuoption.MenuOption;
import user.Person;
import useroption.UserOption;

public class HackerRank {

    private DataBase dataBase;
    private DataBaseProblems dataBaseProblems;
    private MenuFactory menuOptions;
    private final Map<String, Factory> factories;
    private ConsoleInput consoleInput;
    private Person user;
    private boolean logged;

    public HackerRank(DataBase dataBase, DataBaseProblems dataBaseProblems) {
        this.dataBase = dataBase;
        this.dataBaseProblems = dataBaseProblems;
        consoleInput = new ConsoleInput();
        menuOptions = new MenuFactory(consoleInput);
        factories = new HashMap<>();
        factories.put("user", new UserFactory(consoleInput));
        factories.put("guest", new GuestFactory(consoleInput));
        factories.put("admin", new AdminFactory(consoleInput));
        user = null;
        logged = false;
    }

    public void hackerMenu() {
        ConsoleOutput.outputMessage("Hello to HackerRank! Hope you have a great time!");
        while (true) {
            if (logged) {
                if (!user.getType().equals("guest")) {
                    if (user.showNumberOfUnreadMessages() != 0) {
                        ConsoleOutput.outputMessage(
                                String.format("You have %d unread messages", user.showNumberOfUnreadMessages()));
                    }
                }
                List<String> option = Arrays.asList(consoleInput.typeText("User Option: ").trim().split(" "));
                UserOption userOption = null;
                try {
                    userOption = factories.get(user.getType()).getOption(option);
                    if (userOption == null) {
                        break;
                    }
                    String result = userOption.executeOption(user, dataBase, dataBaseProblems, option);
                    if (result.equals("logout")) {
                        user = null;
                        logged = false;
                    } else {
                        ConsoleOutput.outputMessage(result);
                    }
                } catch (Exception e) {
                    ConsoleOutput.outputMessage(e.getMessage());
                }
            } else {
                printMenu();
                List<String> option = Arrays.asList(consoleInput.typeText("Option: ").trim().split(" "));
                MenuOption menuOption = null;
                try {
                    menuOption = menuOptions.getOption(option);
                    if (menuOption == null) {
                        break;
                    }
                    user = menuOption.executeOption(dataBase);
                } catch (Exception e) {
                    ConsoleOutput.outputMessage(e.getMessage());
                }
                if (user != null) {
                    logged = true;
                }
            }
        }
    }

    public void printMenu() {
        ConsoleOutput.outputMessage("(singIn, singUp, guest, quit)");
    }
}
