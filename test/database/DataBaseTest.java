package database;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import user.Person;

public class DataBaseTest {

    @Test
    public void shouldReturnOnlyOneAdminUserWhenANewDataBaseWasCreated() {
        DataBase dataBase = null;
        try {
            dataBase = new DataBase();
        } catch (ClassNotFoundException | IOException e) {
        }
        String result = "admin\n";
        assertEquals(result, dataBase.getUsers());
    }

    @Test
    public void shouldReturnTrueWhenSearchingForAUserThatIsInTheDataBase() {
        DataBase dataBase = null;
        try {
            dataBase = new DataBase();
        } catch (ClassNotFoundException | IOException e) {
        }
        dataBase.addUser("Viktor", "123");
        assertTrue(dataBase.contains("Viktor"));
    }

    @Test
    public void shouldReturnFalseWhenSearchingForAUserThatHasBeenDeletedFromTheDataBase() {
        DataBase dataBase = null;
        try {
            dataBase = new DataBase();
        } catch (ClassNotFoundException | IOException e) {
        }
        dataBase.addUser("Viktor", "123");
        dataBase.deleteUser("Viktor");
        assertFalse(dataBase.contains("Viktor"));
    }

    @Test
    public void shouldReturnTrueWhenCheckingIfPasswordOfASavedUserIsCorrectlyHashed() {
        DataBase dataBase = null;
        try {
            dataBase = new DataBase();
        } catch (ClassNotFoundException | IOException e) {
        }
        dataBase.addUser("Viktor", "123");
        Person person = dataBase.getUser("Viktor");
        assertEquals(81479, person.getPassword());
    }

    @Test
    public void shouldReturnAUserWhenHeIsIntheDataBase() {
        DataBase dataBase = null;
        try {
            dataBase = new DataBase();
        } catch (ClassNotFoundException | IOException e) {
        }
        dataBase.addUser("Viktor", "123");
        Person person = dataBase.getUser("Viktor");
        assertEquals("Viktor", person.getUserName());
    }

    @Test
    public void ShouldReturnThreeUsersThatHaveBeenAddedToTheDataBaseWhenDataBaseHasBeenShutDownAndActivatedAgain() {
        DataBase dataBase = null;
        try {
            dataBase = new DataBase();
        } catch (ClassNotFoundException | IOException e) {
        }
        dataBase.addUser("Viktor", "123");
        dataBase.addUser("Gosho", "123");

        try {
            dataBase.saveAccounts();
        } catch (IOException e1) {
        }

        try {
            dataBase = new DataBase();
        } catch (ClassNotFoundException | IOException e) {
        }

        int result = dataBase.getUsers().split("\n").length;
        dataBase.deleteUser("Viktor");
        dataBase.deleteUser("Gosho");

        try {
            dataBase.saveAccounts();
        } catch (IOException e1) {
        }

        assertEquals(3, result);
    }
}
