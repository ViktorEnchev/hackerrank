package database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Arrays;

import org.junit.Test;

public class DataBaseProblemsTest {

    @Test
    public void shouldReturnTrueWhenSearchingForAProblemByIDThatIsInTheDataBase() {
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        dataBaseProblems.addProblem(Arrays.asList("tag", "name", "explanation", "example", "int", "int a"));
        assertTrue(dataBaseProblems.contains(1));
    }

    @Test
    public void shouldReturnFalseWhenSearchingForAProblemThatHasBeenDeletedFromTheDataBase() {
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        dataBaseProblems.addProblem(Arrays.asList("tag", "name", "explanation", "example", "int", "int a"));
        dataBaseProblems.deleteProblem(1);
        assertFalse(dataBaseProblems.contains(1));
    }

    @Test
    public void shouldReturnTrueWhenCheckingIfTagOfASavedProblemExists() {
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        dataBaseProblems.addProblem(Arrays.asList("tag", "name", "explanation", "example", "int", "int a"));
        assertTrue(dataBaseProblems.constainsTag("tag"));
    }

    @Test
    public void shouldReturnAProblemByIdWhenProblemIsIntheDataBase() {
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        dataBaseProblems.addProblem(Arrays.asList("tag", "name", "explanation", "example", "int", "int a"));
        String result = 1 + "." + "name" + "(#" + "tag" + ")\n" + "\n" + "explanation" + "\n\n" + "example" + "\n";
        assertEquals(result, dataBaseProblems.showProblemByID(1));
    }

    @Test
    public void shouldReturnAllProblemsByTagWhenProblemsAreIntheDataBase() {
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        dataBaseProblems.addProblem(Arrays.asList("tag", "name", "explanation", "example", "int", "int a"));
        dataBaseProblems.addProblem(Arrays.asList("tag", "name1", "explanation1", "example1", "int", "int a"));
        dataBaseProblems.addProblem(Arrays.asList("tag", "name2", "explanation2", "example2", "int", "int a"));
        String result = 1 + "." + "name" + "(#" + "tag" + ")\n" + 2 + "." + "name1" + "(#" + "tag" + ")\n" + 3 + "."
                + "name2" + "(#" + "tag" + ")\n";
        assertEquals(result, dataBaseProblems.showProblemsByTag("tag"));
    }

    @Test
    public void shouldReturn2ProblemsWithSameTagWhenThereAre4ProblemsWith2By2DiffrentTagsIntheDataBase() {
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        dataBaseProblems.addProblem(Arrays.asList("tag", "name", "explanation", "example", "int", "int a"));
        dataBaseProblems.addProblem(Arrays.asList("tag", "name1", "explanation1", "example1", "int", "int a"));
        dataBaseProblems.addProblem(Arrays.asList("tag1", "name2", "explanation2", "example2", "int", "int a"));
        dataBaseProblems.addProblem(Arrays.asList("tag1", "name3", "explanation3", "example3", "int", "int a"));
        String result = 1 + "." + "name" + "(#" + "tag" + ")\n" + 2 + "." + "name1" + "(#" + "tag" + ")\n";
        assertEquals(result, dataBaseProblems.showProblemsByTag("tag"));
    }

    @Test
    public void ShouldReturnThreeProblemsThatHaveBeenAddedToTheDataBaseWhenDataBaseHasBeenShutDownAndActivatedAgain() {
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        dataBaseProblems.addProblem(Arrays.asList("tag", "name", "explanation", "example", "int", "int a"));
        dataBaseProblems.addProblem(Arrays.asList("tag", "name1", "explanation1", "example1", "int", "int a"));
        dataBaseProblems.addProblem(Arrays.asList("tag", "name2", "explanation2", "example2", "int", "int a"));

        try {
            dataBaseProblems.saveProblems();
        } catch (IOException e1) {
        }

        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }

        int result = dataBaseProblems.showAllProblems().split("\n").length;
        dataBaseProblems.deleteProblem(1);
        dataBaseProblems.deleteProblem(2);
        dataBaseProblems.deleteProblem(3);

        dataBaseProblems.setIdToOne();
        try {
            dataBaseProblems.saveProblems();
        } catch (IOException e1) {
        }

        assertEquals(3, result);
    }

}
