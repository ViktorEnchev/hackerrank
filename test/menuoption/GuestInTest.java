package menuoption;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import consolemethod.ConsoleInput;
import database.DataBase;

public class GuestInTest {

    @Test
    public void shouldReturnPersonOfTypeGuestWhenGuestSingsIn() {
        DataBase dataBase = null;
        try {
            dataBase = new DataBase();
        } catch (ClassNotFoundException | IOException e) {
        }
        MenuOption guestIn = new GuestIn(new ConsoleInput());
        assertEquals("guest", guestIn.executeOption(dataBase).getType());
    }
}
