package menuoption;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import consolemethod.ConsoleInput;
import database.DataBase;

public class AdminInTest {

    @Test
    public void shouldReturnPersonOfTypeAdminWhenAdminSingsIn() {
        DataBase dataBase = null;
        try {
            dataBase = new DataBase();
        } catch (ClassNotFoundException | IOException e) {
        }
        MenuOption adminIn = new AdminIn(new ConsoleInput());
        assertEquals("admin", adminIn.executeOption(dataBase).getType());
    }
}
