package menuoption;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

import consolemethod.ConsoleInput;
import database.DataBase;
import user.Person;

public class SingUpTest {

    @Test
    public void shouldReturnNullWhenQuitHasBeenTypedForUserName() {
        ConsoleInput consoleInput = Mockito.mock(ConsoleInput.class);
        MenuOption singUp = new SingUp(consoleInput);
        Mockito.when(consoleInput.typeText("User name: ")).thenReturn("quit");
        DataBase dataBase = null;
        Person person = singUp.executeOption(dataBase);

        assertEquals(null, person);
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenUserTypesAnUserNameThatIsAlreadyInTheDataBase()
            throws IllegalArgumentException {
        ConsoleInput consoleInput = Mockito.mock(ConsoleInput.class);
        MenuOption singUp = new SingUp(consoleInput);
        Mockito.when(consoleInput.typeText("User name: ")).thenReturn("Viktor");
        DataBase dataBase = null;
        try {
            dataBase = new DataBase();
        } catch (ClassNotFoundException | IOException e) {
        }
        dataBase.addUser("Viktor", "123");
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Viktor already exists!");
        @SuppressWarnings("unused")
        Person person = singUp.executeOption(dataBase);
    }

    @Test
    public void shouldReturnTrueWhenDataBaseIsAskedIfItContainsTheNewUserThatHasSingedUp() {
        ConsoleInput consoleInput = Mockito.mock(ConsoleInput.class);
        MenuOption singUp = new SingUp(consoleInput);
        Mockito.when(consoleInput.typeText("User name: ")).thenReturn("Viktor");
        Mockito.when(consoleInput.typeText("Password: ")).thenReturn("123");
        DataBase dataBase = null;
        try {
            dataBase = new DataBase();
        } catch (ClassNotFoundException | IOException e) {
        }
        singUp.executeOption(dataBase);
        assertTrue(dataBase.contains("Viktor"));
    }

    @Test
    public void shouldReturnAnUserWhenTheSingUpWasSuccessful() {
        ConsoleInput consoleInput = Mockito.mock(ConsoleInput.class);
        MenuOption singUp = new SingUp(consoleInput);
        Mockito.when(consoleInput.typeText("User name: ")).thenReturn("Viktor");
        Mockito.when(consoleInput.typeText("Password: ")).thenReturn("123");
        DataBase dataBase = null;
        try {
            dataBase = new DataBase();
        } catch (ClassNotFoundException | IOException e) {
        }
        Person person = singUp.executeOption(dataBase);
        assertEquals("Viktor", person.getUserName());
    }

}
