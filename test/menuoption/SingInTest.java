package menuoption;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import consolemethod.ConsoleInput;
import database.DataBase;
import user.Person;

@RunWith(MockitoJUnitRunner.class)
public class SingInTest {

    @Test
    public void shouldReturnNullWhenQuitHasBeenTypedForUserName() {
        ConsoleInput consoleInput = Mockito.mock(ConsoleInput.class);
        MenuOption singIn = new SingIn(consoleInput);
        Mockito.when(consoleInput.typeText("User name: ")).thenReturn("quit");
        DataBase dataBase = null;
        Person person = singIn.executeOption(dataBase);

        assertEquals(null, person);
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenUserIsNotInTheDataBase() throws IllegalArgumentException {
        ConsoleInput consoleInput = Mockito.mock(ConsoleInput.class);
        MenuOption singIn = new SingIn(consoleInput);
        Mockito.when(consoleInput.typeText("User name: ")).thenReturn("Viktor");
        DataBase dataBase = null;
        try {
            dataBase = new DataBase();
        } catch (ClassNotFoundException | IOException e) {
        }
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Viktor doesn't exist!");
        @SuppressWarnings("unused")
        Person person = singIn.executeOption(dataBase);
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenUsersPasswordIsIncorect() throws IllegalArgumentException {
        ConsoleInput consoleInput = Mockito.mock(ConsoleInput.class);
        MenuOption singIn = new SingIn(consoleInput);
        Mockito.when(consoleInput.typeText("User name: ")).thenReturn("Viktor");
        Mockito.when(consoleInput.typeText("Password: ")).thenReturn("234");
        DataBase dataBase = null;
        try {
            dataBase = new DataBase();
        } catch (ClassNotFoundException | IOException e) {
        }
        dataBase.addUser("Viktor", "123");
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Wrong password!");
        @SuppressWarnings("unused")
        Person person = singIn.executeOption(dataBase);
    }

    @Test
    public void shouldReturnAnUserWhenSingedInIntoAnExistingAccount() {
        ConsoleInput consoleInput = Mockito.mock(ConsoleInput.class);
        MenuOption singIn = new SingIn(consoleInput);
        Mockito.when(consoleInput.typeText("User name: ")).thenReturn("Viktor");
        Mockito.when(consoleInput.typeText("Password: ")).thenReturn("123");
        DataBase dataBase = null;
        try {
            dataBase = new DataBase();
        } catch (ClassNotFoundException | IOException e) {
        }
        dataBase.addUser("Viktor", "123");
        Person person = singIn.executeOption(dataBase);
        assertEquals("Viktor", person.getUserName());
    }
}
