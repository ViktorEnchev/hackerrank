package factory;

import java.util.Arrays;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import consolemethod.ConsoleInput;

public class FactoryTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenAnInvalidOptionHasBeenTyped() throws IllegalArgumentException {
        Factory factory = new AdminFactory(new ConsoleInput());
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Wrong input!");
        factory.getOption(Arrays.asList("Incorect command"));
    }
}
