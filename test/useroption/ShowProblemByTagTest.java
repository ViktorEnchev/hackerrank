package useroption;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Arrays;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import consolemethod.ConsoleInput;
import database.DataBaseProblems;

public class ShowProblemByTagTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenInputCommandsAreWrong() throws IllegalArgumentException {
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        UserOption showProblemByTag = new ShowProblemByTag(new ConsoleInput());
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Wrong input!");
        showProblemByTag.executeOption(null, null, dataBaseProblems, Arrays.asList("wrong input"));
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenThereIsNoProblemWithTheTypedTag()
            throws IllegalArgumentException {
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        UserOption showProblemById = new ShowProblemByTag(new ConsoleInput());
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("There are no tag problems!");
        showProblemById.executeOption(null, null, dataBaseProblems, Arrays.asList("showProblemByTag", "tag"));
    }

    @Test
    public void shouldReturnProblemsByTagWhenTheProblemsAreIntheDataBase() {
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        dataBaseProblems.addProblem(Arrays.asList("tag", "name", "explanation", "example", "int", "int a"));
        dataBaseProblems.addProblem(Arrays.asList("tag", "name1", "explanation1", "example1", "int", "int a"));
        dataBaseProblems.addProblem(Arrays.asList("tag", "name2", "explanation2", "example2", "int", "int a"));
        UserOption showProblemByTag = new ShowProblemByTag(new ConsoleInput());
        String result = 1 + "." + "name" + "(#" + "tag" + ")\n" + 2 + "." + "name1" + "(#" + "tag" + ")\n" + 3 + "."
                + "name2" + "(#" + "tag" + ")\n";
        assertEquals(result,
                showProblemByTag.executeOption(null, null, dataBaseProblems, Arrays.asList("showProblemByTag", "tag")));
    }
}
