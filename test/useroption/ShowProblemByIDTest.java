package useroption;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Arrays;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import consolemethod.ConsoleInput;
import database.DataBaseProblems;

public class ShowProblemByIDTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenInputCommandsAreWrong() throws IllegalArgumentException {
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        UserOption showProblemById = new ShowProblemByID(new ConsoleInput());
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Wrong input!");
        showProblemById.executeOption(null, null, dataBaseProblems, Arrays.asList("wrong input"));
    }

    @Test
    public void shouldThrowNumberFormatExceptionWhenInputNumberIsNotANumber() throws NumberFormatException {
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        UserOption showProblemById = new ShowProblemByID(new ConsoleInput());
        thrown.expect(NumberFormatException.class);
        thrown.expectMessage("Not a number!");
        showProblemById.executeOption(null, null, dataBaseProblems, Arrays.asList("showProblemByID", "a"));
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenThereIsNoProblemWithTheTypedId()
            throws IllegalArgumentException {
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        UserOption showProblemById = new ShowProblemByID(new ConsoleInput());
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Problem 2 doesn't exist!");
        showProblemById.executeOption(null, null, dataBaseProblems, Arrays.asList("showProblemByID", "2"));
    }

    @Test
    public void shouldReturnAProblemByIdWhenProblemIsIntheDataBase() {
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        dataBaseProblems.addProblem(Arrays.asList("tag", "name", "explanation", "example", "int", "int a"));
        UserOption showProblemById = new ShowProblemByID(new ConsoleInput());
        String result = 1 + "." + "name" + "(#" + "tag" + ")\n" + "\n" + "explanation" + "\n\n" + "example" + "\n";
        assertEquals(result,
                showProblemById.executeOption(null, null, dataBaseProblems, Arrays.asList("showProblemByID", "1")));
    }
}
