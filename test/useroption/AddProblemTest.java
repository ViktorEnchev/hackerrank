package useroption;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import consolemethod.ConsoleInput;
import database.DataBaseProblems;

@RunWith(MockitoJUnitRunner.class)
public class AddProblemTest {

    @Test
    public void shouldReturnProblemAddedToDataBaseWhenNewProblemIsAdded() {
        ConsoleInput consoleInput = Mockito.mock(ConsoleInput.class);
        UserOption addProblem = new AddProblem(consoleInput);
        Mockito.when(consoleInput.typeText("Tag: ")).thenReturn("tag");
        Mockito.when(consoleInput.typeText("Name: ")).thenReturn("name");
        Mockito.when(consoleInput.typeText("Explanation: ")).thenReturn("explanation");
        Mockito.when(consoleInput.typeText("Example: ")).thenReturn("example");
        Mockito.when(consoleInput.typeText("Return Type: ")).thenReturn("int");
        Mockito.when(consoleInput.typeText("Input Data: ")).thenReturn("int a");
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        String result = "Problem added to data base";
        assertEquals(result, addProblem.executeOption(null, null, dataBaseProblems,
                Arrays.asList("tag", "name", "explanation", "example", "int", "int a")));
    }

    @Test
    public void shouldReturnTrueWhenANewProblemIsAddedAndThenCheckIfItIsInDataBase() {
        ConsoleInput consoleInput = Mockito.mock(ConsoleInput.class);
        UserOption addProblem = new AddProblem(consoleInput);
        Mockito.when(consoleInput.typeText("Tag: ")).thenReturn("tag");
        Mockito.when(consoleInput.typeText("Name: ")).thenReturn("name");
        Mockito.when(consoleInput.typeText("Explanation: ")).thenReturn("explanation");
        Mockito.when(consoleInput.typeText("Example: ")).thenReturn("example");
        Mockito.when(consoleInput.typeText("Return Type: ")).thenReturn("int");
        Mockito.when(consoleInput.typeText("Input Data: ")).thenReturn("int a");
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        addProblem.executeOption(null, null, dataBaseProblems,
                Arrays.asList("tag", "name", "explanation", "example", "int", "int a"));
        assertTrue(dataBaseProblems.contains(1));
    }
}
