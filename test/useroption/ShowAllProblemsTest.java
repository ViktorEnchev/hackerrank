package useroption;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Arrays;

import org.junit.Test;

import consolemethod.ConsoleInput;
import database.DataBaseProblems;

public class ShowAllProblemsTest {

    @Test
    public void shouldReturnAllProblemsINTheDataBase() {
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        dataBaseProblems.addProblem(Arrays.asList("tag", "name", "explanation", "example", "int", "int a"));
        dataBaseProblems.addProblem(Arrays.asList("tag", "name1", "explanation1", "example1", "int", "int a"));
        dataBaseProblems.addProblem(Arrays.asList("tag", "name2", "explanation2", "example2", "int", "int a"));
        UserOption showProblems = new ShowAllProblems(new ConsoleInput());
        String result = 1 + "." + "name" + "(#" + "tag" + ")\n" + 2 + "." + "name1" + "(#" + "tag" + ")\n" + 3 + "."
                + "name2" + "(#" + "tag" + ")\n";
        assertEquals(result, showProblems.executeOption(null, null, dataBaseProblems, null));
    }

    @Test
    public void shouldReturnEmptyStringWhenNoProblemsAreInTheDataBase() {
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        UserOption showProblems = new ShowAllProblems(new ConsoleInput());
        String result = "";
        assertEquals(result, showProblems.executeOption(null, null, dataBaseProblems, null));
    }
}
