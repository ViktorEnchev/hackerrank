package useroption;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

import consolemethod.ConsoleInput;
import database.DataBase;
import user.User;

public class DeleteUserProfileTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenTheUserIsNotInTheDataBase() throws IllegalArgumentException {
        ConsoleInput consoleInput = Mockito.mock(ConsoleInput.class);
        Mockito.when(consoleInput.typeText("User to delete: ")).thenReturn("Viktor");
        UserOption deleteUser = new DeleteUserProfile(consoleInput);
        DataBase dataBase = null;
        try {
            dataBase = new DataBase();
        } catch (ClassNotFoundException | IOException e) {
        }
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Viktor doesn't exist!");
        deleteUser.executeOption(new User("Viktor", 123), dataBase, null, null);
    }

    @Test
    public void shouldReturnFalseWhenUserHasBeenDeletedThenCheckIfHeIsInDataBase() {
        ConsoleInput consoleInput = Mockito.mock(ConsoleInput.class);
        Mockito.when(consoleInput.typeText("User to delete: ")).thenReturn("Viktor");
        UserOption deleteUser = new DeleteUserProfile(consoleInput);
        DataBase dataBase = null;
        try {
            dataBase = new DataBase();
        } catch (ClassNotFoundException | IOException e) {
        }
        dataBase.addUser("Viktor", "123");
        deleteUser.executeOption(new User("Viktor", 123), dataBase, null, null);
        assertFalse(dataBase.contains("Viktor"));
    }

}
