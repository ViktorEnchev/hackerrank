package useroption;

import java.io.IOException;
import java.util.Arrays;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import consolemethod.ConsoleInput;
import database.DataBaseProblems;

public class SolveProblemTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenInputCommandsAreWrong() throws IllegalArgumentException {
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        UserOption solveProblem = new SolveProblem(new ConsoleInput());
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Wrong input!");
        solveProblem.executeOption(null, null, dataBaseProblems, Arrays.asList("wrong input"));
    }

    @Test
    public void shouldThrowNumberFormatExceptionWhenInputNumberIsNotANumber() throws NumberFormatException {
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        UserOption solveProblem = new SolveProblem(new ConsoleInput());
        thrown.expect(NumberFormatException.class);
        thrown.expectMessage("Not a number!");
        solveProblem.executeOption(null, null, dataBaseProblems, Arrays.asList("solveProblem", "a", "java"));
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenThereIsNoProblemWithTheTypedId()
            throws IllegalArgumentException {
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        UserOption solveProblem = new SolveProblem(new ConsoleInput());
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Problem 2 doesn't exist!");
        solveProblem.executeOption(null, null, dataBaseProblems, Arrays.asList("solveProblem", "2", "java"));
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenTheProgrammingLanguageTypedIsNorCorrect()
            throws IllegalArgumentException {
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        UserOption solveProblem = new SolveProblem(new ConsoleInput());
        dataBaseProblems.addProblem(Arrays.asList("basic", "name", "explanation", "example", "int", "int a"));
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("solveProblem <id> <solution>(file or (java, cpp)");
        solveProblem.executeOption(null, null, dataBaseProblems, Arrays.asList("solveProblem", "1", "c"));
    }
}
