package useroption;

import java.io.IOException;
import java.util.Arrays;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import consolemethod.ConsoleInput;
import database.DataBase;
import user.User;

public class WriteMessageTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenCommandsAreWrong() throws IllegalArgumentException {
        UserOption writeMessage = new WriteMessage(new ConsoleInput());
        DataBase dataBase = null;
        try {
            dataBase = new DataBase();
        } catch (ClassNotFoundException | IOException e) {
        }
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Wrong input!");
        writeMessage.executeOption(new User("Viktor", 123), dataBase, null, Arrays.asList("wrong commmand"));
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenCommandsAreFewerThanExpected() throws IllegalArgumentException {
        UserOption writeMessage = new WriteMessage(new ConsoleInput());
        DataBase dataBase = null;
        try {
            dataBase = new DataBase();
        } catch (ClassNotFoundException | IOException e) {
        }
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Wrong input!");
        writeMessage.executeOption(new User("Viktor", 123), dataBase, null, Arrays.asList("writeMessage", "Viktor"));
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenTheUserToWriteTheMessageToIsNotInTheDataBase()
            throws IllegalArgumentException {
        UserOption writeMessage = new WriteMessage(new ConsoleInput());
        DataBase dataBase = null;
        try {
            dataBase = new DataBase();
        } catch (ClassNotFoundException | IOException e) {
        }
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Viktor doesn't exist!");
        writeMessage.executeOption(new User("Viktor", 123), dataBase, null, Arrays.asList("writeMessage", "Viktor", "hi"));
    }
}
