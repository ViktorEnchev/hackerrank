package useroption;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Arrays;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import consolemethod.ConsoleInput;
import database.DataBase;
import user.User;

public class ShowUserTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenInputCommandsAreWrong() throws IllegalArgumentException {
        UserOption showUser = new ShowUser(new ConsoleInput());
        DataBase dataBase = null;
        try {
            dataBase = new DataBase();
        } catch (ClassNotFoundException | IOException e) {
        }
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Wrong input!");
        showUser.executeOption(new User("Viktor", 123), dataBase, null, Arrays.asList("wrong commmand"));
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenTheUserToShowIsNotInTheDataBase()
            throws IllegalArgumentException {
        UserOption showUser = new ShowUser(new ConsoleInput());
        DataBase dataBase = null;
        try {
            dataBase = new DataBase();
        } catch (ClassNotFoundException | IOException e) {
        }
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Viktor doesn't exist!");
        showUser.executeOption(new User("Viktor", 123), dataBase, null, Arrays.asList("showUser", "Viktor"));
    }

    @Test
    public void shouldShowTheUsersInformationWhenShowUserHasBeenInvokedForAnExistingUser() {
        UserOption showUser = new ShowUser(new ConsoleInput());
        DataBase dataBase = null;
        try {
            dataBase = new DataBase();
        } catch (ClassNotFoundException | IOException e) {
        }
        dataBase.addUser("Viktor", "123");
        String expected = "Name: Viktor\n";
        String result = showUser.executeOption(new User("Viktor", 123), dataBase, null, Arrays.asList("showUser", "Viktor"));
        assertEquals(expected, result);
    }
}
