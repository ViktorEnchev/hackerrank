package useroption;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.io.IOException;
import java.util.Arrays;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import consolemethod.ConsoleInput;
import database.DataBaseProblems;

public class DeleteProblemTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenInputCommandsAreWrong() throws IllegalArgumentException {
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        UserOption deleteProblem = new DeleteProblem(new ConsoleInput());
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Wrong input!");
        deleteProblem.executeOption(null, null, dataBaseProblems, Arrays.asList("wrong input"));
    }

    @Test
    public void shouldThrowNumberFormatExceptionWhenInputNumberIsNotANumber() throws NumberFormatException {
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        UserOption deleteProblem = new DeleteProblem(new ConsoleInput());
        thrown.expect(NumberFormatException.class);
        thrown.expectMessage("Not a number!");
        deleteProblem.executeOption(null, null, dataBaseProblems, Arrays.asList("showProblemByID", "a"));
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenThereIsNoProblemWithTheTypedId()
            throws IllegalArgumentException {
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        UserOption deleteProblem = new DeleteProblem(new ConsoleInput());
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Problem 2 doesn't exist!");
        deleteProblem.executeOption(null, null, dataBaseProblems, Arrays.asList("showProblemByID", "2"));
    }

    @Test
    public void shouldReturnFalseWhenSearchingForAProblemThatHasBeenDeleted() {
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        dataBaseProblems.addProblem(Arrays.asList("tag", "name", "explanation", "example", "int", "int a"));
        UserOption deleteProblem = new DeleteProblem(new ConsoleInput());
        deleteProblem.executeOption(null, null, dataBaseProblems, Arrays.asList("showProblemByID", "1"));
        assertFalse(dataBaseProblems.contains(1));
    }

    @Test
    public void shouldReturnProblem1HasBeenDeletedWhenExecutingTheDeleteProblemOption() {
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        dataBaseProblems.addProblem(Arrays.asList("tag", "name", "explanation", "example", "int", "int a"));
        UserOption deleteProblem = new DeleteProblem(new ConsoleInput());
        String result = "Problem 1 has been deleted";
        assertEquals(result,
                deleteProblem.executeOption(null, null, dataBaseProblems, Arrays.asList("showProblemByID", "1")));
    }
}
