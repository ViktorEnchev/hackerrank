package useroption;

import static org.junit.Assert.assertFalse;

import java.io.IOException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import consolemethod.ConsoleInput;
import database.DataBase;
import user.User;

public class DeleteMyProfileTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenTheUserIsNotInTheDataBase() throws IllegalArgumentException {
        UserOption deleteProfile = new DeleteMyProfile(new ConsoleInput());
        DataBase dataBase = null;
        try {
            dataBase = new DataBase();
        } catch (ClassNotFoundException | IOException e) {
        }
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Viktor doesn't exist!");
        deleteProfile.executeOption(new User("Viktor", 123), dataBase, null, null);
    }

    @Test
    public void shouldReturnFalseWhenUserHasDeletedHisAccountThenCheckIfHeIsInDataBase() {
        UserOption deleteProfile = new DeleteMyProfile(new ConsoleInput());
        DataBase dataBase = null;
        try {
            dataBase = new DataBase();
        } catch (ClassNotFoundException | IOException e) {
        }
        dataBase.addUser("Viktor", "123");
        deleteProfile.executeOption(new User("Viktor", 123), dataBase, null, null);
        assertFalse(dataBase.contains("Viktor"));
    }
}
