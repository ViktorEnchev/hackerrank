package solveproblem;

import static org.junit.Assert.assertEquals;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

import consolemethod.ConsoleInput;
import database.DataBaseProblems;

public class SolveProblemCppTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenCPlusPlusFileDoesntExist() throws IllegalArgumentException {
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        SolveProblemLanguage solveCpp = new SolveProblemCpp(new ConsoleInput());
        dataBaseProblems.addProblem(Arrays.asList("basic", "name", "explanation", "example", "int", "int a"));
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("No file nonexisting.cpp");
        solveCpp.execute(dataBaseProblems.getProblem(1), Arrays.asList("", "", "nonexisting.cpp"));
    }

    @Test
    public void shouldCompileWhenGoodCPlusPlusCodeIsParsed() {
        ConsoleInput consoleInput = Mockito.mock(ConsoleInput.class);
        Mockito.when(consoleInput.typeText("Your solution:\n")).thenReturn("return a%2;}");
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        SolveProblemLanguage solveCpp = new SolveProblemCpp(consoleInput);
        dataBaseProblems.addProblem(Arrays.asList("basic", "name", "explanation", "example", "int", "int a"));
        String actual = solveCpp.execute(dataBaseProblems.getProblem(1), Arrays.asList("SolveProblem", "1", "cpp"));
        String result = "Compilation is successful";
        assertEquals(result, actual);
    }

    @Test
    public void shouldNotCompileWhenCPlusPlusCodeHasErrors() {
        ConsoleInput consoleInput = Mockito.mock(ConsoleInput.class);
        Mockito.when(consoleInput.typeText("Your solution:\n")).thenReturn("return a%2}}");
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        SolveProblemLanguage solveCpp = new SolveProblemCpp(consoleInput);
        dataBaseProblems.addProblem(Arrays.asList("basic", "name", "explanation", "example", "int", "int a"));
        String actual = solveCpp.execute(dataBaseProblems.getProblem(1), Arrays.asList("SolveProblem", "1", "cpp"));
        String result = "Compilation Failed";
        assertEquals(result, actual);
    }

    @Test
    public void shouldNotCompileWhenBadCPlusPlusFileIsParsed() {
        File file = new File("wrong.cpp");
        try {
            file.createNewFile();
        } catch (IOException e1) {
        }
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file))) {
            bufferedWriter.write("#include<iostream>\nint main(){return");
        } catch (IOException e1) {
        }
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        SolveProblemLanguage solveCpp = new SolveProblemCpp(new ConsoleInput());
        dataBaseProblems.addProblem(Arrays.asList("basic", "name", "explanation", "example", "int", "int a"));
        String actual = solveCpp.execute(dataBaseProblems.getProblem(1),
                Arrays.asList("SolveProblem", "1", "wrong.cpp"));
        String result = "Compilation Failed";
        file.delete();
        assertEquals(result, actual);
    }

    @Test
    public void shouldCompileWhenGoodCPlusPlusFileIsParsed() {
        File file = new File("right.cpp");
        try {
            file.createNewFile();
        } catch (IOException e1) {
        }
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file))) {
            bufferedWriter.write("#include<iostream>\nint main(){return 0;}");
        } catch (IOException e1) {
        }
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        SolveProblemLanguage solveCpp = new SolveProblemCpp(new ConsoleInput());
        dataBaseProblems.addProblem(Arrays.asList("basic", "name", "explanation", "example", "int", "int a"));
        String actual = solveCpp.execute(dataBaseProblems.getProblem(1),
                Arrays.asList("SolveProblem", "1", "right.cpp"));
        String result = "Compilation is successful";
        file.delete();
        new File("right").delete();
        assertEquals(result, actual);
    }
}
