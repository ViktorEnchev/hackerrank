package solveproblem;

import static org.junit.Assert.assertEquals;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

import consolemethod.ConsoleInput;
import database.DataBaseProblems;

public class SolveProblemJavaTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenJavaFileDoesntExist() throws IllegalArgumentException {
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        SolveProblemLanguage solveJava = new SolveProblemJava(new ConsoleInput());
        dataBaseProblems.addProblem(Arrays.asList("basic", "name", "explanation", "example", "int", "int a"));
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("No file nonexisting.java");
        solveJava.execute(dataBaseProblems.getProblem(1), Arrays.asList("", "", "nonexisting.java"));
    }

    @Test
    public void shouldCompileWhenGoodJavaCodeIsParsed() {
        ConsoleInput consoleInput = Mockito.mock(ConsoleInput.class);
        Mockito.when(consoleInput.typeText("Your solution:\n")).thenReturn("return a%2;}}");
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        SolveProblemLanguage solveJava = new SolveProblemJava(consoleInput);
        dataBaseProblems.addProblem(Arrays.asList("basic", "name", "explanation", "example", "int", "int a"));
        String actual = solveJava.execute(dataBaseProblems.getProblem(1), Arrays.asList("SolveProblem", "1", "java"));
        String result = "Compilation is successful";
        assertEquals(result, actual);
    }

    @Test
    public void shouldNotCompileWhenJavaCodeHasErrors() {
        ConsoleInput consoleInput = Mockito.mock(ConsoleInput.class);
        Mockito.when(consoleInput.typeText("Your solution:\n")).thenReturn("return a%2}}");
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        SolveProblemLanguage solveJava = new SolveProblemJava(consoleInput);
        dataBaseProblems.addProblem(Arrays.asList("basic", "name", "explanation", "example", "int", "int a"));
        String actual = solveJava.execute(dataBaseProblems.getProblem(1), Arrays.asList("SolveProblem", "1", "java"));
        String result = "Compilation Failed";
        assertEquals(result, actual);
    }

    @Test
    public void shouldNotCompileWhenBadJavaFileIsParsed() {
        File file = new File("wrong.java");
        try {
            file.createNewFile();
        } catch (IOException e1) {
        }
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        SolveProblemLanguage solveJava = new SolveProblemJava(new ConsoleInput());
        dataBaseProblems.addProblem(Arrays.asList("basic", "name", "explanation", "example", "int", "int a"));
        String actual = solveJava.execute(dataBaseProblems.getProblem(1),
                Arrays.asList("SolveProblem", "1", "wrong.java"));
        String result = "Compilation Failed";
        file.delete();
        assertEquals(result, actual);
    }

    @Test
    public void shouldCompileWhenGoodJavaFileIsParsed() {
        File file = new File("right.java");
        try {
            file.createNewFile();
        } catch (IOException e1) {
        }
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file))) {
            bufferedWriter.write("public class right {}");
        } catch (IOException e1) {
        }
        DataBaseProblems dataBaseProblems = null;
        try {
            dataBaseProblems = new DataBaseProblems();
        } catch (ClassNotFoundException | IOException e) {
        }
        SolveProblemLanguage solveJava = new SolveProblemJava(new ConsoleInput());
        dataBaseProblems.addProblem(Arrays.asList("basic", "name", "explanation", "example", "int", "int a"));
        String actual = solveJava.execute(dataBaseProblems.getProblem(1),
                Arrays.asList("SolveProblem", "1", "right.java"));
        String result = "Compilation is successful";
        file.delete();
        new File("right.class").delete();
        assertEquals(result, actual);
    }
}
