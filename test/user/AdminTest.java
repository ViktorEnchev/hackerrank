package user;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AdminTest {

    @Test
    public void shouldReturn2WhenAskedHowManyUnreadMessagesTheAdminHas() {
        Person admin = new Admin("admin", 123);
        admin.setMessage("Viktor", "hello");
        admin.setMessage("Viktor", "it's me");
        assertEquals(2, admin.showNumberOfUnreadMessages());
    }

    @Test
    public void shouldReturn4WhenAskedHowManyUnreadMessagesTheAdminHas() {
        Person admin = new Admin("admin", 123);
        admin.setMessage("Viktor", "hello");
        admin.setMessage("Viktor", "it's me");
        admin.setMessage("Gosho", "How are you");
        admin.setMessage("Gosho", "Im Fine Thanks");
        assertEquals(4, admin.showNumberOfUnreadMessages());
    }

    @Test
    public void shouldReturnTheUnreadMessagesWhenAdminWantsToSeeThem() {
        Person admin = new Admin("admin", 123);
        admin.setMessage("Viktor", "hello");
        admin.setMessage("Viktor", "it's me");
        String result = "Viktor:\n" + "hello\n\n" + "Viktor:\n" + "it's me\n\n";
        assertEquals(result, admin.getAllUnreadMessages());
    }

    @Test
    public void shouldReturnTheUnreadMessagesFromAllPeopleWhenAdminWantsToSeeThem() {
        Person admin = new Admin("admin", 123);
        admin.setMessage("Viktor", "hello");
        admin.setMessage("Viktor", "it's me");
        admin.setMessage("Gosho", "How are you");
        admin.setMessage("Gosho", "Im Fine Thanks");
        String result = "Viktor:\n" + "hello\n\n" + "Viktor:\n" + "it's me\n\n" + "Gosho:\n" + "How are you\n\n"
                + "Gosho:\n" + "Im Fine Thanks\n\n";
        assertEquals(result, admin.getAllUnreadMessages());
    }

    @Test
    public void shouldReturn0WhenAdminReadsHisUnreadMessagesThenAsksHowManyUndreadMessagesHeHas() {
        Person admin = new Admin("admin", 123);
        admin.setMessage("Viktor", "hello");
        admin.setMessage("Viktor", "it's me");
        admin.getAllUnreadMessages();
        assertEquals(0, admin.showNumberOfUnreadMessages());
    }
}
