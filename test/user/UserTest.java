package user;

import static org.junit.Assert.*;

import org.junit.Test;

public class UserTest {

    @Test
    public void shouldReturn2WhenAskedHowManyUnreadMessagesTheUserHas() {
        Person user = new User("Ivan", 123);
        user.setMessage("Viktor", "hello");
        user.setMessage("Viktor", "it's me");
        assertEquals(2, user.showNumberOfUnreadMessages());
    }

    @Test
    public void shouldReturn4WhenAskedHowManyUnreadMessagesTheUserHas() {
        Person user = new User("Ivan", 123);
        user.setMessage("Viktor", "hello");
        user.setMessage("Viktor", "it's me");
        user.setMessage("Gosho", "How are you");
        user.setMessage("Gosho", "Im Fine Thanks");
        assertEquals(4, user.showNumberOfUnreadMessages());
    }

    @Test
    public void shouldReturnTheUnreadMessagesWhenAdminWantsToSeeThem() {
        Person user = new User("Ivan", 123);
        user.setMessage("Viktor", "hello");
        user.setMessage("Viktor", "it's me");
        String result = "Viktor:\n" + "hello\n\n" + "Viktor:\n" + "it's me\n\n";
        assertEquals(result, user.getAllUnreadMessages());
    }

    @Test
    public void shouldReturnTheUnreadMessagesFromAllPeopleWhenUserWantsToSeeThem() {
        Person user = new User("Ivan", 123);
        user.setMessage("Viktor", "hello");
        user.setMessage("Viktor", "it's me");
        user.setMessage("Gosho", "How are you");
        user.setMessage("Gosho", "Im Fine Thanks");
        String result = "Viktor:\n" + "hello\n\n" + "Viktor:\n" + "it's me\n\n" + "Gosho:\n" + "How are you\n\n"
                + "Gosho:\n" + "Im Fine Thanks\n\n";
        assertEquals(result, user.getAllUnreadMessages());
    }

    @Test
    public void shouldReturn0WhenUserReadsHisUnreadMessagesThenAsksHowManyUndreadMessagesHeHas() {
        Person user = new User("Ivan", 123);
        user.setMessage("Viktor", "hello");
        user.setMessage("Viktor", "it's me");
        user.getAllUnreadMessages();
        assertEquals(0, user.showNumberOfUnreadMessages());
    }
}
